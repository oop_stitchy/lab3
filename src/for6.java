public class for6 {
    public static void main(String[] args) {
        System.out.println("For");
        for (int i = 0;i < 5;i++){
            for (int j = 0;j < 5;j++){
                System.out.print("*");
            }
            System.out.println();
        }
        System.out.println("While");
        int i = 0;
        while (i<5) {
            int j = 0;
            while (j<5) {
                System.out.print("*");
                j++;
            }
            System.out.println();
            i++;
            
        }
    }
}
