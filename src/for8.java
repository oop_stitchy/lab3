public class for8 {
    
    public static void main(String[] args) {
        System.out.println("For");
        for (int i = 1;i <= 5;i++){
            for (int j = 1;j <= 5;j++){
                System.out.print(j);
            }
            System.out.println();
        }
        System.out.println("While");
        int i = 1;
        while (i<6) {
            int j = 1;
            while (j<6) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
            
        }
    }
}
