import java.util.Scanner;

public class for18 {
    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            int num = scanner.nextInt();
            if (num == 1) {
                System.out.print("Please input number:");
                int num2 = scanner.nextInt();
                for (int i = 0;i < num2;i++){
                    for (int j = 0;j <= i;j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (num == 2) {
                System.out.print("Please input number:");
                int num2 = scanner.nextInt();
                for (int i = num2;i >= 1;i--){
                    for (int j = 0;j < i;j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (num == 3) {
                System.out.print("Please input number:");
                int num2 = scanner.nextInt();
                for (int i = 0;i < num2;i++){
                    for (int j = 0;j < i;j++){
                        System.out.print(" ");
                    }
                    for (int k = 5;k>i;k--){
                        System.out.print("*");
                    }
                    System.out.println();
                }
                
            }
            else if (num == 4){
                System.out.print("Please input number:");
                int num2 = scanner.nextInt();
                for (int i = 0;i < num2;i++){
                    for (int j = 0;j <= num2 -1 - i;j++){
                        System.out.print(" ");
                    }
                    for (int k = 0;k<=i;k++){
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (num == 5) {
                System.out.println("Bye bye!!!");
                break;
            }
            else{
                System.out.println("Error: Please input number between 1-5");

            }

        }
    }
}

