import java.util.Scanner;

public class for3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input first number : ");
        int number1 = scanner.nextInt();
        System.out.print("Please input second number : ");
        int number2 = scanner.nextInt();
        if (number1 <= number2) {
           for (int i = number1;i<=number2;i++){
            System.out.print(i+" ");
           }
        }
        else {
            System.out.println("Error");
        }
        scanner.close();
    }
}
