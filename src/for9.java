import java.util.Scanner;

public class for9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input n : ");
        int n = scanner.nextInt();
        System.out.println("For");
        for (int i = 1;i <= n;i++){
            for (int j = 1;j <= n;j++){
                System.out.print(j);
            }
            System.out.println();
        }
        System.out.println("While");
        int i = 1;
        while (i<=n) {
            int j = 1;
            while (j<=n) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
            
        }
        scanner.close();
    }
    
} 

