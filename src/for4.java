import java.util.Scanner;

public class for4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int count = 0;
        while (true) {
            System.out.print("Please input number : ");
            int number = scanner.nextInt();
            if (number == 0) {
                System.out.println("Bye");
                break;
            }
            sum += number;
            count++;
            double avg = sum/count;

            System.out.println("Sum : "+sum+" "+"Avg : "+avg);
        }
        scanner.close();

    }
}
